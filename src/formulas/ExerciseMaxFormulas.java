package formulas;

/**
 * This class has the 1 rep max and N rep max calculations
 */
public class ExerciseMaxFormulas {

    /**
     * Max calculation error codes
     */
    public static final int INVALID_REPS = -1 ; // Error code for invalid reps
    public static final int INVALID_WEIGHT = -2 ; // Error code for invalid weight
    public static final int INVALID_REPS_AND_WEIGHT = -3 ; // Error code for both invalid reps and weight
    public static final int NOT_IMPLEMENTED = -4 ; // Error code when the method isn't implemented
    public static final int INVALID_FORMULA = -5; // Error code for the wrong formula

    /**
     * The different one rep max formulas
     */
    public enum OneRepMaxFormula
    {

        EPLEY

    }

    /**
     * Calculates the 1 rep max with the chosen formula
     *
     * @param reps - The number of repetitions performed
     * @param weight - The weight lifted
     *
     * @return double - If the reps and weight are valid, returns the one rep max.
     *                  Else if both the reps and weight are invalid, returns INVALID_rEPS_AND_weIGht.
     *                  Else if only the reps are invalid, returns INVALID_REPS.
     *                  Else if only the weight is invalid, returns INVALID_WEIGHT.
     */
    public static double calcOneRepMax( OneRepMaxFormula formula, int reps, double weight )
    {

        double oneRepMax ;

        // Choose the right formula
        switch( formula )
        {

            case EPLEY :

                oneRepMax = epleyOneRepMaxFormula( reps, weight ) ;
                break;

            // If there was an invalid or unimplemented formula
            default :

                oneRepMax = INVALID_FORMULA ;

        }

        return oneRepMax ;

    }

    /**
     * Calculates the 1 rep max using the Epley formula
     *
     * @param reps - The number of reps performed
     * @param weight - The weight lifted
     *
     * @return double - If the reps and weight are valid, returns the one rep max.
     *                  If both the reps and weight are invalid, returns INVALID_REPS_AND_WEIGHT.
     *                  If only the reps are invalid, returns INVALID_REPS.
     *                  If only the weight is invalid, returns INVALID_WEIGHT.
     *
     */
    public static double epleyOneRepMaxFormula( int reps, double weight )
    {

        boolean isValidRep = reps > 1 ; // Whether the reps are valid
        boolean isValidWeight = weight > 0 ; // Whether the weight is valid

        // Make sure the reps and weight are valid
        if( isValidRep && isValidWeight)
        {

            // Calculate the one rep max with the Epley formula
            return weight * (1 + (reps / 30.0));

        }

        // If both the reps and the weight are invalid
        else if ( !isValidRep && !isValidWeight )
        {

            return INVALID_REPS_AND_WEIGHT ;

        }

        // Check if only the reps are invalid
        else if ( !isValidRep )
        {

            return INVALID_REPS ;

        }

        // Check if only the weight is invalid
        else
        {

            return INVALID_WEIGHT ;

        }


    }

    /**
     * Calculates the N rep max
     * 
     * @param targetRepCount - The rep count to calculate the max for
     * @param oneRepMax - The most weight you can lift once
     *
     * @return double - If the target rep count and the one rep max are valid,
     *                      returns the max weight for the target rep count.
     *                  If both the target rep count and the one rep max is invalid, returns INVALID_REPS_AND_WEIGHT.
     *                  If only the target rep count is invalid, returns INVALID_REPS.
     *                  If only the one rep max is invalid, returns INVALID_WEIGHT.
     */
    public static double calcMaxRep( int targetRepCount, double oneRepMax )
    {

        // TODO : Implement
        return NOT_IMPLEMENTED ;

    }
}
